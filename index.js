import express from "express";
import expressGraphQL from "express-graphql";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import cors from "cors";

import schema from "./graphql/";

const app = express();
const PORT = process.env.PORT || "2000";
const db = "mongodb://rajarjind:" + encodeURIComponent("rajarjind@20") + "@ds153974.mlab.com:53974/graphql-mongo";


mongoose.connect(db, { useNewUrlParser: true }, function (err) {
    if (err) {
        console.log('err....', err);
    } else {
        console.log("Mongodb Connected!!!");
    }
})
// mongoose
//   .connect(
//     db,
//     {
//       useCreateIndex: true,
//       useNewUrlParser: true
//     }
//   )
//   .then(() => console.log("MongoDB connected"))
//   .catch(err => console.log(err));

app.use(
    "/graphql",
    cors(),
    bodyParser.json(),
    expressGraphQL({
        schema,
        graphiql: true
    })
);

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));