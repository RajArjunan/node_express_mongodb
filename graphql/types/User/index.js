
export default `
  type User {
    id: String!
    name: String!
    email: String!
    password: String!
  }
  type Query {
    user(id: String!): User
    users: [User]
  }
  type Mutation {
    addUser(id: String!, name: String!, email: String!, password: String!): User
    editUser(id: String, name: String, email: String): User
    deleteUser(id: String, name: String, email: String, password: String): User
    login(email: String!, password: String!): User
  }
`;